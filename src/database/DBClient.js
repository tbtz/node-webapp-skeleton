const {Pool} = require('pg');
const dbConfig = require('../configurations/DBConfig');

module.exports = new Pool(dbConfig);
