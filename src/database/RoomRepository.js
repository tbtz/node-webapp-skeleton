//const DBClient = require('./DBClient');

class RoomService {
    findById(roomId) {
        if (roomId !== '1') {
            throw new Error('Oh no, cant find room with id ' + roomId)
        } else {
            return Promise.resolve({name: 'room1'});
        }
        //return DBClient.query('SELECT * FROM rooms WHERE id = $1', [roomId]);
    }
}

module.exports = RoomService;
