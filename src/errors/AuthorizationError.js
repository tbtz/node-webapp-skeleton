class AuthorizationError extends Error {
    constructor(...args) {
        super(...args)
        Error.captureStackTrace(this, AuthorizationError)
    }
}

module.exports = AuthorizationError;
