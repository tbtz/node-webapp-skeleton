const express = require('express');
const router = express.Router();

const {Role} = require('../constants');
const authorize = require('../middlewares/securityMiddlewares/Authorize');

const userController = require('./UserController');
const roomController = require('./RoomController');
const meetingController = require('./MeetingController');
const authController = require('./AuthController');

router.use('/auth', authController);
router.use('/users', userController);
router.use('/rooms', authorize(Role.Admin), roomController);
router.use('/meetings', meetingController);

module.exports = router;
