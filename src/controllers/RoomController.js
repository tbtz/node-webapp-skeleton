const express = require('express');
const router = express.Router();

const RoomRepository = require('../database/RoomRepository');
const RoomService = require('../services/RoomService');

const roomService = new RoomService(new RoomRepository());

router.get('/:id', async (req, res, next) => {
    try {
        const roomId = req.params.id;
        const room = await roomService.getRoomById(roomId);
        res.json(room);
    } catch (e) {
        next(e)
    }
});

module.exports = router;
