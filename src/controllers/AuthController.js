const express = require('express');
const router = express.Router();

const AuthService = require('../services/AuthService');

router.post('/', (req, res, next) => {
    let username = req.body.username;
    let password = req.body.password;

    try {
        let jwt = AuthService.authenticate(username, password);
        res.json(jwt);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
