const AuthorizationError = require('../../errors/AuthorizationError');

const AuthService = require('../../services/AuthService');

module.exports = authorize;

function authorize(roles = []) {
    if (typeof roles === 'string') {
        roles = [roles];
    }

    return (req, res, next) => {
        let jwt = req.headers['Authorization'] || req.headers['authorization'];
        jwt = jwt.replace('Bearer ', '');

        // TODO: verify JWT

        // TODO: Read Role from JWT
        let roleFromJWT = 'USER';

        if (roles.length && !roles.includes(roleFromJWT)) {
            next(new AuthorizationError('Not authorized for this method'))
        } else {
            next();
        }
    }
}
