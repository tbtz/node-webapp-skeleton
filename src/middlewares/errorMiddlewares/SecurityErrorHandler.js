const AuthorizationError = require('../../errors/AuthorizationError');
const AuthenticationError = require('../../errors/AuthenticationError');

function securityErrorHandler(err, req, res, next) {
    if (err instanceof AuthenticationError) {
        res.status(401);
    }
    if (err instanceof AuthorizationError) {
        res.status(403);
    }
    next(err);
}

module.exports = securityErrorHandler;
