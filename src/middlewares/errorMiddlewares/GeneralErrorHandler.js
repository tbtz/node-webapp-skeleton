function generalErrorHandler(err, req, res, next) {
    if (!res.statusCode || res.statusCode === 200) {
        res.status(500);
    }
    res.send({message: err.message});
}

module.exports = generalErrorHandler;
