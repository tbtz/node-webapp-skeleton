const AuthenticationError = require('../errors/AuthenticationError');

class AuthService {
    authenticate(username, password) {
        if (username !== 'testUser' || password !== 'testPassword') {
            throw new AuthenticationError('Wrong credentials');
        }
        // TODO: generate JWT

        return 'generatedJWT';
    }
}

module.exports = new AuthService();
