class RoomService {
    constructor(roomRepository) {
        this.roomRepository = roomRepository;
    }

    getRoomById(roomId) {
        return this.roomRepository.findById(roomId);
    }
}

module.exports = RoomService;
