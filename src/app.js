const PORT = process.env.PORT || 3000;

const express = require('express');
const bodyParser = require('body-parser');

const api = require('./controllers');
const errorLogger = require('./middlewares/errorMiddlewares/ErrorLogger');
const generalErrorHandler = require('./middlewares/errorMiddlewares/GeneralErrorHandler');
const securityErrorHandler = require('./middlewares/errorMiddlewares/SecurityErrorHandler');

const app = express();

app.use(bodyParser.json());

app.use('/api', api);

app.use('*', express.static(__dirname + '/public'));

app.use(errorLogger);
app.use(securityErrorHandler);
app.use(generalErrorHandler);


app.listen(PORT, function () {
    console.log(`App listening on port ${PORT}!`);
});
