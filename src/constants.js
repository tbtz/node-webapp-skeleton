module.exports = {
    Role: Object.freeze({
        Admin: 'ADMIN',
        User: 'USER'
    })
};
